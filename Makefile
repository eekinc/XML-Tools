PIP=pip3

docs:
	mkdir -p documentation
	rm -f documentation/*.html
	pydoc3 -w ./ > documentation/log.txt
	mv *.html documentation
	rm *.pyc

install:
	$(PIP) install --upgrade .

clean:
	@rm -rf .coverage
	@rm -rf lambda_tmp
	@rm -rf tmp.zip

test: clean
	python3 `which nosetests` --nocapture --with-coverage --cover-package=spreadsheet
	coverage report -m --skip-covered