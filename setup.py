#!/usr/bin/env python3

from os import environ
from setuptools import setup, find_packages
from setuptools.command.install import install
from subprocess import check_call as execute

repoDependencies = [
    ('git+https://gitlab.com/eekinc/MockUtils', '25bc8504637f9da5b4ad6f5c7231f6e2a310c818')
]

class PostEekWebAppBackendInstallCommand(install):

    def run(self):
        for repo in repoDependencies:
            target = environ.get('INSTALL_TARGET', None)
            if isinstance(repo, tuple):
                repo = '{}@{}'.format(*repo)
            
            if target is not None:
                print('pip install {} --target={}'.format(repo, target))
                execute('pip install --upgrade {} --target={}'.format(repo, target), shell=True)
            else:
                execute('pip install --upgrade {}'.format(repo).split())

        install.run(self)


setup(name='XML-Tools',
    version='1.0',
    description='',
    author='Eek, Inc.',
    author_email='',
    url='',
    packages=find_packages(),
    install_requires=[
        'nose',
        'nose-exclude',
        'coverage'
    ],
    cmdclass={
        'install': PostEekWebAppBackendInstallCommand
    }
)
