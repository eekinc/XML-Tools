import zipfile
from xml.etree.ElementTree import iterparse


class XLSX:
    '''
    Used to parse XLSX documents.
    '''

    _SHARED_STRINGS = 'xl/sharedStrings.xml'
    _SHEET1 = 'xl/worksheets/sheet1.xml'

    # https://stackoverflow.com/a/22067980
    def parse(self, filename, hasHeader=False):
        z = zipfile.ZipFile(filename)
        
        with z.open(XLSX._SHARED_STRINGS) as sharedStrings:
            strings = self._parseSharedStrings(sharedStrings)
        
        with z.open(XLSX._SHEET1) as sheet1:
            return self._parseSheet1(sheet1, strings, hasHeader)
    
    def _parseSharedStrings(self, sharedStrings):
        return [element.text for _, element in iterparse(sharedStrings) if element.tag.endswith('}t')]
        
    def _parseSheet1(self, sheet1, strings, hasHeader):
        rows = []
        
        i = 0
        value = ''
        header = {}
        row = {}

        for _, element in iterparse(sheet1):
            # <v>84</v>
            if element.tag.endswith('}v'):
                value = element.text
            
            # <c r="A3" t="s"><v>84</v></c>
            if element.tag.endswith('}c'):
                if element.attrib.get('t') == 's':
                    value = strings[int(value)]
                
                # AZ22
                columnKey = element.attrib['r']
                while columnKey[-1].isdigit():
                    columnKey = columnKey[:-1]
                
                if hasHeader:
                    if i == 0:
                        header[columnKey] = value
                    else:
                        key = header[columnKey]
                        row[key] = value
                else:
                    row[columnKey] = value
                    
                value = ''
            
            if element.tag.endswith('}row'):
                if not hasHeader or i != 0:
                    rows.append(row)
                row = {}
                i += 1
                    
        return rows