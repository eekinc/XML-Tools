from mockutils.IsolatedTest import IsolatedTest
import os
import unittest
from spreadsheet.XLSX import XLSX

class test_XLSX(IsolatedTest):
    
    _TESTDATA_FILENAME = os.path.join(os.path.dirname(__file__), 'Metrc-Oregon-Packages-Active.xlsx')
    
    def test_parse(self):
        parser = XLSX()
        data = parser.parse(test_XLSX._TESTDATA_FILENAME)
        
        expected = {
            'A': 'Tag',
            'B': 'Harvest',
            'C': 'Item',
            'D': 'Category',
            'E': 'Quantity',
            'F': "<abbr title='Production Batch'>P.B.</abbr>",
            'G': "<abbr title='Production Batch'>P.B.</abbr> No.",
            'H': 'Lab Testing', 'I': "<abbr title='Administrative Hold'>A.H.</abbr>",
            'J': "<abbr title='Packaged Date'>Date</abbr>",
            'K': "<abbr title='Received'>Rcv'd</abbr>"
        }
        actual = data[0]
        self.assertEquals(expected, actual)
        
        expected = {
            'A': '0X0000000000000000000000',
            'B': 'Bar 12/31/17',
            'C': 'Bar Buds',
            'D': 'Buds (by strain)',
            'E': '454',
            'F': '0',
            'H': 'TestPassed',
            'I': '0',
            'J': '43045',
            'K': '2017-11-09T22:58:48+00:00'}
        actual = data[2]
        self.assertEquals(expected, actual)
        
    def test_parseWithHeader(self):
        parser = XLSX()
        hasHeader = True
        data = parser.parse(test_XLSX._TESTDATA_FILENAME, hasHeader)
        
        expected = {
            'Tag': '0X0000000000000000000000',
            'Harvest': 'Bar 12/31/17',
            'Item': 'Bar Buds',
            'Category': 'Buds (by strain)',
            'Quantity': '454',
            "<abbr title='Production Batch'>P.B.</abbr>": '0',
            # G is missing
            'Lab Testing': 'TestPassed',
            "<abbr title='Administrative Hold'>A.H.</abbr>": '0',
            "<abbr title='Packaged Date'>Date</abbr>": '43045',
            "<abbr title='Received'>Rcv'd</abbr>": '2017-11-09T22:58:48+00:00'}
        actual = data[1]
        self.assertEquals(expected, actual)

if __name__ == '__main__':
    unittest.main()
